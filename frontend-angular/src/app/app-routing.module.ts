import { StoreAddMedicineComponent } from './store/component/store-add-medicine/store-add-medicine.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDashboardComponent } from './user/component/user-dashboard/user-dashboard.component';
import { AdminDashboardComponent } from './admin/component/admin-dashboard/admin-dashboard.component';
import { AdminLoginComponent } from './login/admin-login/admin-login.component';
import { UserLoginComponent } from './login/user-login/user-login.component';
import { StoreLoginComponent } from './login/store-login/store-login.component';
import { SearchComponent } from './common/component/search/search.component';
import { StoreRegisterComponent } from './store/component/store-register/store-register.component';
import { ContactUsComponent } from './common/component/contact-us/contact-us.component';
import { AboutUsComponent } from './common/component/about-us/about-us.component';
import { UserProfileComponent } from './user/component/user-profile/user-profile.component';
import { ManageStoreComponent } from './admin/component/manage-store/manage-store.component';
import { ManageQueriesComponent } from './admin/component/manage-queries/manage-queries.component';
import { StoreDashBoardComponent } from './store/component/store-dash-board/store-dash-board.component';

import { NotfoundComponent } from './common/component/notfound/notfound.component';
import { HomeComponent } from './admin/component/home/home.component';
import { StoreManageMedicineComponent } from './store/component/store-manage-medicine/store-manage-medicine.component';
import { StoreProfileComponent } from './store/component/store-profile/store-profile.component';
export const routes: Routes = [

  { path: '', component: SearchComponent },
  { path: 'admin', component: AdminLoginComponent },
  {
    path: 'admin/dashboard', component: AdminDashboardComponent,
    children: [
      { path: '', component: ManageStoreComponent },
      { path: 'summary', component: HomeComponent },
      { path: 'managestore', component: ManageStoreComponent },
      { path: 'managequeries', component: ManageQueriesComponent },
      { path: '', redirectTo: 'admin/dashboard', pathMatch: 'full' }
    ]
  },
  { path: 'user-login', component: UserLoginComponent },
  { path: 'user-login/dashboard', component: UserDashboardComponent },
  { path: 'user-login/user-profile', component: UserProfileComponent },
  { path: 'store-login', component: StoreLoginComponent },
  { path: 'store-login/storedashboard/store-add-medicine', component: StoreAddMedicineComponent },
  {path : 'store-login/storeRegistration', component : StoreRegisterComponent},
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'aboutus', component: AboutUsComponent },
  { path: 'store-login/storedashboard', component: StoreDashBoardComponent },
  { path: 'store-manage-medicine', component: StoreManageMedicineComponent },
  { path: 'store-login/store-profile', component: StoreProfileComponent}

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [AdminLoginComponent, AdminDashboardComponent];

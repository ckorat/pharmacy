import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IStores } from 'src/app/common/serivces/stores';
import { StoreServicesService } from '../../services/store-services.service';
import { PopupService } from '../../../common/serivces/popup.service';

@Component({
  selector: 'app-store-profile',
  templateUrl: './store-profile.component.html',
  styleUrls: ['./store-profile.component.css']
})
export class StoreProfileComponent implements OnInit {
  profileForm: FormGroup;
  storedetails: IStores;
  changePasswordForm: FormGroup;
  flag = false;
  flag1 = false;
  storeid: number;
  constructor(private profileFormBuilder: FormBuilder, private storeservice: StoreServicesService,
              public popup: PopupService) {

    this.profileForm = this.profileFormBuilder.group({
      storename: ['', Validators.required],
      ownername: ['', Validators.required],
      contact: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[7-9]{1}[0-9]{9}')]],
      emailid: [{ value: '', disabled: true }],
      pincode: [{ value: '', disabled: true }],
      address: [{ value: '', disabled: true }]
    });
    this.changePasswordForm = this.profileFormBuilder.group({
      oldPass: ['', [Validators.required]],
      newPass: ['', [Validators.required, Validators.minLength(8)]],
      confirmPass: ['', [Validators.required]]
    });
    this.passwordFormFields.confirmPass.valueChanges.subscribe(data =>
      (data === this.passwordFormFields.newPass.value) ? this.flag1 = true : this.flag1 = false);

    this.storedetails = this.storeservice.getAllDetails();
    this.storeid = this.storedetails.sID;
    this.profileForm.patchValue({
      storename: this.storedetails.sName,
      ownername: this.storedetails.sOwnerName,
      contact: this.storedetails.sContact,
      emailid: this.storedetails.sEmailID,
      address: this.storedetails.sAddress,
      pincode: this.storedetails.sPincode
    });
  }
  onSaveProfile() {

    this.storedetails = this.profileForm.value;
    this.storeservice.updateStoreDetails(this.storedetails, this.storeid);
    this.popup.show('success', 'Changes Made Successfully');
  }

  onSavePassword() {
    const oldPassword = this.changePasswordForm.controls.oldPass.value;
    if (this.storeservice.checkOldPassword(this.storeid, oldPassword)) {

      const newPassword = this.changePasswordForm.controls.newPass.value;
      this.storeservice.updateStorePassword(this.storeid, oldPassword, newPassword);
      this.popup.show('success', 'Password Has been Changed');
    } else {
      this.popup.show('error', 'Old Password is not Correct. Could not change password');
      this.changePasswordForm.reset();
    }
  }
  get formFields() { return this.profileForm.controls; }
  get passwordFormFields() { return this.changePasswordForm.controls; }
  ngOnInit() {
  }
  logout() {
    this.storeservice.logout();
  }
}

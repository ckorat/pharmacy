import { Component, OnInit } from '@angular/core';
import { StoreServicesService } from '../../services/store-services.service'
import { IStores } from 'src/app/common/serivces/stores';
import { Router } from '@angular/router';
import { LoginServiceService } from 'src/app/common/serivces/login-service.service';
import { IOrder } from 'src/app/common/serivces/order';
import { IOrderDetails } from '../../model/IOrderDetails';
import { IUsers } from 'src/app/common/serivces/users';
import { IMedicines } from 'src/app/common/serivces/medicines';
import { MedicineService } from 'src/app/common/serivces/medicine.service';
@Component({
  selector: 'app-store-dash-board',
  templateUrl: './store-dash-board.component.html',
  styleUrls: ['./store-dash-board.component.css']
})
export class StoreDashBoardComponent {
  stores: IStores;
  ordersDisplayFormat: IOrderDetails[];
  Orders: IOrder[];
  Medicines: IMedicines[];
  allOrders: IOrderDetails[];
  requestedOrder: number;
  completedOrder: number;
  rejectedOrder: number;
  constructor(private storeservice: StoreServicesService, private router: Router,
              private loginservice: LoginServiceService, private medicineservice: MedicineService) {
    if (this.storeservice.checkSession()) {
      this.router.navigateByUrl('/store-login');
    } else {
      this.stores = this.storeservice.getAllDetails();
      this.Orders = this.storeservice.getOrdersFromLocalStorage();
      this.ordersDisplayFormat = this.getOrderDetails(this.stores.sID);
      this.requestedOrder = this.storeservice.getRequestedOrderCount(this.stores.sID);
      this.completedOrder = this.storeservice.getCompletedOrderCount(this.stores.sID);
      this.rejectedOrder = this.storeservice.getRejectedOrderCount(this.stores.sID);
    }

  }
  confirmorder(orderid, medicineid) {
    const index = this.Orders.findIndex(data => data.oId === orderid);
    this.Orders[index].oStatus = 'Completed';
    this.storeservice.setOrderToLocalStorage(this.Orders);
    this.Medicines = this.medicineservice.getMedicineFromLocalStorage();
    const medicineindex = this.Medicines.findIndex(data => data.mID === medicineid);
    this.requestedOrder = this.storeservice.getRequestedOrderCount(this.stores.sID);
    this.completedOrder = this.storeservice.getCompletedOrderCount(this.stores.sID);
    this.Medicines[medicineindex].mQTY = (this.Medicines[medicineindex].mQTY) - (this.Orders[index].oQTY);
    this.medicineservice.setMedicinesToLocalStorage(this.Medicines);
    this.ordersDisplayFormat = this.getOrderDetails(this.stores.sID);

  }
  rejectorder(orderid) {
    const index = this.Orders.findIndex(data => data.oId === orderid);
    this.Orders[index].oStatus = 'Canceled';
    this.storeservice.setOrderToLocalStorage(this.Orders);
    this.ordersDisplayFormat = this.getOrderDetails(this.stores.sID);
    this.rejectedOrder = this.storeservice.getRejectedOrderCount(this.stores.sID);
    this.requestedOrder = this.storeservice.getRequestedOrderCount(this.stores.sID);
  }
  logout() {
    this.storeservice.logout();
  }
  gotoAddMedicine() {
    this.router.navigateByUrl('/store-manage-medicine');
  }
  getOrderDetails(storeId): IOrderDetails[] {
    const order: IOrder[] = this.storeservice.getOrderByStoreId(storeId);
    const users: IUsers[] = this.loginservice.getUserFromLocalStorage();
    let user: IUsers;
    const OrderDetailsArray = new Array<IOrderDetails>();
    for (const iterator of order) {
      const orderDetail = new IOrderDetails();
      orderDetail.id = new IUsers();
      user = users.find(data => data.id === iterator.id);
      orderDetail.id.emailid = user.emailid;
      orderDetail.oId = iterator.oId;
      orderDetail.id.address = user.address;
      orderDetail.id.fname = user.fname;
      orderDetail.id.lname = user.lname;
      orderDetail.medicineid = iterator.mID;
      orderDetail.mName = this.storeservice.getMedicineName(iterator.mID);
      orderDetail.oQTY = iterator.oQTY;
      orderDetail.oDate = iterator.oDate;
      orderDetail.oStatus = iterator.oStatus;
      orderDetail.stock = this.medicineservice.checkStockofMedicine(iterator.mID);
      OrderDetailsArray.push(orderDetail);
    }
    this.allOrders = OrderDetailsArray;
    return this.allOrders;
  }
}

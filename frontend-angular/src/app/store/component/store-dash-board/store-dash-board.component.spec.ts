import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreDashBoardComponent } from './store-dash-board.component';

describe('StoreDashBoardComponent', () => {
  let component: StoreDashBoardComponent;
  let fixture: ComponentFixture<StoreDashBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreDashBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreDashBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

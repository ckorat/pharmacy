import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PasswordValidator } from '../../../common/Validations/confirmPassword.validator';
import { IStores } from 'src/app/common/serivces/stores';
import { LoginServiceService } from 'src/app/common/serivces/login-service.service';
import { StoreServicesService } from '../../services/store-services.service';
import { PopupService } from '../../../common/serivces/popup.service';

@Component({
  selector: 'app-store-register',
  templateUrl: './store-register.component.html',
  styleUrls: ['./store-register.component.css']
})
export class StoreRegisterComponent {
  flagOfConfirmpwd = false;
  registrationform: FormGroup;
  constructor(private fb: FormBuilder, private router: Router,
              private loginservice: LoginServiceService, private storeservice: StoreServicesService,
              public popup: PopupService) {

    this.registrationform = this.fb.group({
      storename: ['', Validators.required],
      ownername: ['', Validators.required],
      contact: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[7-9]{1}[0-9]{9}')]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      cnfpassword: ['', [Validators.required]],
      storeadd: ['', Validators.required],
      pincode: ['', [Validators.required, Validators.pattern('[0-9]{5}.'), Validators.maxLength(6), Validators.minLength(6)]]
    });
    this.fval.cnfpassword.valueChanges.subscribe(data =>
      (data === this.fval.password.value) ? this.flagOfConfirmpwd = true : this.flagOfConfirmpwd = false);
  }



  saveRegistrationData = function() {
    if (this.registrationform.invalid) {
      this.popup.show('error', 'Invalid Submission');
      return;
    }
    if (this.storeservice.checkEmail(this.registrationform.get('email').value)) {
      this.popup.show('error', 'This Email Already exist. Please Register with another Email');
      return;
    }
    const registrationData: IStores = {
      sID: this.loginservice.getLengthStore() + 1,
      sName: this.registrationform.get('storename').value,
      sOwnerName: this.registrationform.get('ownername').value,
      sContact: this.registrationform.get('contact').value,
      sEmailID: this.registrationform.get('email').value,
      sPassword: this.registrationform.get('password').value,
      sAddress: this.registrationform.get('storeadd').value,
      sPincode: +this.registrationform.get('pincode').value,
      sStatus: false,
    };
    this.loginservice.storeRegistration(registrationData);
    this.popup.show('success', 'You Are successfully Registered. Please Login to continue');
    this.router.navigateByUrl('/store-login');
  };
  get fval() {
    return this.registrationform.controls;
  }
  gotoLogin() {
    this.router.navigateByUrl('/store-login');
  }
  resetform() {
    this.registrationform.reset();
  }
}

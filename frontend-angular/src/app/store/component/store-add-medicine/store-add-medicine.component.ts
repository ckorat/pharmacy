import { IMedicines } from './../../../common/serivces/medicines';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StoreServicesService } from '../../services/store-services.service';
import { IStores } from '../../../common/serivces/stores';
import { MedicineService } from 'src/app/common/serivces/medicine.service';
import { PopupService } from '../../../common/serivces/popup.service';

@Component({
  selector: 'app-store-add-medicine',
  templateUrl: './store-add-medicine.component.html',
  styleUrls: ['./store-add-medicine.component.css']
})
export class StoreAddMedicineComponent implements OnInit {
  addMedicineForm: FormGroup;
  addMedicineValidation = false;
  store: IStores;
  checkDate = { dates: '' };

  constructor(private formBuilder: FormBuilder,
              private medicineService: MedicineService,
              private router: Router, private storeservice: StoreServicesService,
              public popup: PopupService) {
    if (this.storeservice.checkSession()) {
      this.router.navigateByUrl('/store-login');
    }
    if (JSON.parse(localStorage.getItem('store')).sStatus === false) {
      this.router.navigateByUrl('/store-login');
    }
    this.store = JSON.parse(localStorage.getItem('store'));
  }

  ngOnInit() {

    this.addMedicineForm = this.formBuilder.group({
      mName: ['', Validators.required],
      mDescription: ['', Validators.required],
      mCompanyName: ['', Validators.required],
      mType: ['', Validators.required],
      mMFD: ['', Validators.required],
      mEXD: ['', Validators.required],
      mQTY: ['', Validators.required],
      mPrice: ['', Validators.required]
    }, { validator: this.dateLessThan('mMFD', 'mEXD') });
    this.medicineService.fetchAllMedicines();

  }
  get formControlNames() {
    return this.addMedicineForm.controls;
  }

  addMedicineDetails() {
    this.addMedicineValidation = true;

    if (this.addMedicineForm.invalid) {
      return;
    }
    const medicineData: IMedicines = {
      mID: this.medicineService.getLengthMedicine() + 1,
      mName: this.addMedicineForm.get('mName').value,
      sID: this.store.sID,
      mDescription: this.addMedicineForm.get('mDescription').value,
      mCompanyName: this.addMedicineForm.get('mCompanyName').value,
      mType: this.addMedicineForm.get('mType').value,
      mMFD: this.addMedicineForm.get('mMFD').value,
      mEXD: this.addMedicineForm.get('mEXD').value,
      mQTY: +this.addMedicineForm.get('mQTY').value,
      mPrice: +this.addMedicineForm.get('mPrice').value
    };
    this.medicineService.addMedicineDetails(medicineData);
    this.popup.show('success', 'Your Medicine Added');
    this.clearMedicineDetails();
    this.router.navigateByUrl('/store-manage-medicine');
  }

  clearMedicineDetails() {
    this.addMedicineValidation = false;
    this.addMedicineForm.reset();
  }
  logout() {
    this.storeservice.logout();
    this.router.navigateByUrl('/store-login');
  }

  dateLessThan(mMFD: string, mEXD: string) {
    const chckDate = (group: FormGroup): { [key: string]: any } => {
      const manufacturingDate = group.controls[mMFD];
      const expiryDate = group.controls[mEXD];
      if (expiryDate.value !== '' && manufacturingDate.value > expiryDate.value) {
        this.checkDate.dates = 'Manufacturing Date should be greater than Expiry Date';
        return;
      }
      this.checkDate.dates = '';
    };
    return chckDate;
  }
}

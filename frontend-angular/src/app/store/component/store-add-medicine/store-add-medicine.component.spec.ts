import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreAddMedicineComponent } from './store-add-medicine.component';

describe('StoreAddMedicineComponent', () => {
  let component: StoreAddMedicineComponent;
  let fixture: ComponentFixture<StoreAddMedicineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreAddMedicineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreAddMedicineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

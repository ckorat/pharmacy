import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreManageMedicineComponent } from './store-manage-medicine.component';

describe('StoreManageMedicineComponent', () => {
  let component: StoreManageMedicineComponent;
  let fixture: ComponentFixture<StoreManageMedicineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreManageMedicineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreManageMedicineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

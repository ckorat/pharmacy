import { IMedicines } from './../../../common/serivces/medicines';
import { Component, OnInit } from '@angular/core';
import { IStores } from '../../../common/serivces/stores';
import { Router } from '@angular/router';
import { StoreServicesService } from '../../services/store-services.service';
import { MedicineService } from 'src/app/common/serivces/medicine.service';

@Component({
  selector: 'app-store-manage-medicine',
  templateUrl: './store-manage-medicine.component.html',
  styleUrls: ['./store-manage-medicine.component.css']
})
export class StoreManageMedicineComponent implements OnInit {

  manageMedicines: IMedicines[];
  public stores: IStores;
  constructor(private router: Router, private medicineService: MedicineService,private storeservice : StoreServicesService) {
    if (this.storeservice.checkSession()) {
      this.router.navigateByUrl('/store-login');
    } else {
      this.stores = this.storeservice.getAllDetails();
    }
  }

  ngOnInit() {
    this.medicineService.fetchAllMedicines();
    this.manageMedicines = this.medicineService.getMedicineByStoreId(this.stores.sID);
  }
  delete(id: number, storeId: number) {
    storeId = this.stores.sID;
    this.medicineService.deleteMedicines(id, storeId);
    this.ngOnInit();
  }
  updateMedicines(id: number, qty: number, storeId: number) {
    storeId = this.stores.sID;
    this.medicineService.updateMedicines(id, qty, storeId);
    this.ngOnInit();
    this.router.navigateByUrl('/store-login/storedashboard');
  }
  logout() {
    this.storeservice.logout();
    this.router.navigateByUrl('/store-login');
  }
}

import { Router, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreDashBoardComponent } from './component/store-dash-board/store-dash-board.component';
import { StoreRegisterComponent } from './component/store-register/store-register.component';
import { StoreAddMedicineComponent } from './component/store-add-medicine/store-add-medicine.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreManageMedicineComponent } from './component/store-manage-medicine/store-manage-medicine.component';
import { StoreProfileComponent } from './component/store-profile/store-profile.component';

@NgModule({
  declarations: [StoreDashBoardComponent, StoreRegisterComponent, StoreAddMedicineComponent, StoreManageMedicineComponent, StoreProfileComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class StoreModuleModule { }

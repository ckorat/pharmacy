import { Injectable } from '@angular/core';
import { IOrder } from '../../common/serivces/order'
import { Observable, Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IMedicines } from 'src/app/common/serivces/medicines';
import { IStores } from 'src/app/common/serivces/stores';
import { LoginServiceService } from 'src/app/common/serivces/login-service.service';

@Injectable({
  providedIn: 'root'
})
export class StoreServicesService {

  private url = '/assets/data/order.json';
  key = 'Orders';
  keyofstore = 'Store';
  ds: Subscription;
  medicine: IMedicines[];
  orders: IOrder[];
  constructor(private http: HttpClient, private loginservice: LoginServiceService) {
    if (localStorage.getItem('Orders') === null) {
      this.getAllOrders();
    }
  }

  getAllDetails() {
    return JSON.parse(localStorage.getItem('store'));
  }
  checkSession(): boolean {
    if (localStorage.getItem('store') === null) {
      return true;
    }
    return false;
  }
  getAllOrders() {
    this.ds = this.http.get<IOrder>(this.url)
      .subscribe(data => (
        localStorage.setItem(this.key, JSON.stringify(data))
      ));
  }
  setOrderToLocalStorage(orders: IOrder[]) {
    localStorage.setItem(this.key, JSON.stringify(orders));
  }
  getOrdersFromLocalStorage(): IOrder[] {
    return JSON.parse(localStorage.getItem(this.key));
  }
  getOrderByStoreId(storeId: number): IOrder[] {
    const order = this.getOrdersFromLocalStorage();
    return order.filter(data => data.sID === storeId);
  }

  getMedicinefromLocalStorage(): IMedicines[] {
    return JSON.parse(localStorage.getItem('MEDICINES'));
  }
  getMedicineName(mId: number): string {
    const medicine = this.getMedicinefromLocalStorage().find(data => data.mID === mId);
    return medicine.mName;
  }
  getRequestedOrderCount(storeId: number): number {
    this.orders = this.getOrdersFromLocalStorage().filter(data => (data.sID === storeId && data.oStatus === 'Requested'));
    return this.orders.length;
  }
  getCompletedOrderCount(storeId: number): number {
    this.orders = this.getOrdersFromLocalStorage().filter(data => (data.sID === storeId && data.oStatus === 'Completed'));
    return this.orders.length;
  }
  getRejectedOrderCount(storeId: number): number {
    this.orders = this.getOrdersFromLocalStorage().filter(data => (data.sID === storeId && data.oStatus === 'Canceled'));
    return this.orders.length;
  }
  logout() {
    localStorage.removeItem('store');
  }

  updateStoreDetails(store, storeid: number) {
    const stores: IStores[] = JSON.parse(localStorage.getItem(this.keyofstore));
    const index = stores.findIndex(data => data.sID === storeid);
    stores[index].sName = store.storename;
    stores[index].sOwnerName = store.ownername;
    stores[index].sContact = store.contact;
    localStorage.setItem(this.keyofstore, JSON.stringify(stores));
  }
  updateStorePassword(id: number, oldPassword: string, newPassword: string) {
    const stores: IStores[] = JSON.parse(localStorage.getItem(this.keyofstore));
    const index = stores.findIndex(data => data.sID === id);
    stores[index].sPassword = newPassword;
    localStorage.setItem(this.keyofstore, JSON.stringify(stores));
    localStorage.setItem('store', JSON.stringify(stores[index]));
  }
  checkOldPassword(storeid: number, oldPassword: string): boolean {
    const stores: IStores[] = JSON.parse(localStorage.getItem(this.keyofstore));
    const index = stores.findIndex(data => data.sID === storeid);
    if (oldPassword === stores[index].sPassword) {
      return true;
    }
    return false;
  }
  checkEmail(email: string): boolean {
    const allStores = this.loginservice.getStoresFromLocalStorage();
    const oldemail = allStores.filter(data => data.sEmailID === email);
    if (oldemail.length > 0) {
      return true;
    }
    return false;
  }
}

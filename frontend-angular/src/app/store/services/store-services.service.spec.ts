import { TestBed } from '@angular/core/testing';

import { StoreServicesService } from './store-services.service';

describe('StoreServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StoreServicesService = TestBed.get(StoreServicesService);
    expect(service).toBeTruthy();
  });
});

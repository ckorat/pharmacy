import { IUsers } from 'src/app/common/serivces/users';

export class IOrderDetails {
    oId:number;
    sID: number;
    medicineid:number;
    mName: string;
    id: IUsers;
    oQTY: number;
    oDate: Date;
    oDDate: Date;
    oStatus: string;
    stock : number;
}
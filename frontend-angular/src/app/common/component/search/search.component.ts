import { SearchMedsService } from './../../serivces/search-meds.service';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { IUsers } from '../../serivces/users';
import { LoginServiceService } from '../../serivces/login-service.service';
import { IStores } from '../../serivces/stores';
import { StoreServicesService } from 'src/app/store/services/store-services.service';
import { IOrder } from '../../serivces/orders';
import { MedicineService } from '../../serivces/medicine.service';
import { AdminServiceService } from '../../../admin/services/admin-service.service';
import { QueryServiceService } from '../../serivces/query-service.service';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  user: IUsers;
  inputSeach: string;
  displayStores: IStores[] = [];
  showStores: IStores[] = [];
  searchCompleted = false;
  medicinePrice = 0;
  medicineQty = 0;
  showOrder = true;
  storeID;
  constructor(
    private router: Router,
    private searchMeds: SearchMedsService,
    // For Loadind All Users Data in LocalStorage
    private loginServiceService: LoginServiceService,
    private storeService: StoreServicesService,
    // Setting All Medicines to LocalStorage
    private medicineService: MedicineService,
    private queryService: QueryServiceService
  ) {}
  ngOnInit(): void {
    this.user = this.getUser();
  }
  onseachClick() {
    if (!this.inputSeach) {
      alert('Please Insert Something');
    } else {

      if (this.user === null) {
        this.router.navigate(['/user-login']);
      } else {
        this.showStores = [];
        this.searchCompleted = true;
        this.displayStores = this.searchMeds.checkMedsAvailiblity(this.inputSeach);
        if (this.displayStores.length > 0) {
          this.showStores = this.searchMeds.showStores(this.displayStores, this.inputSeach);
        }
      }
    }
  }
  getUser() {
    return JSON.parse(localStorage.getItem('user'));
  }
  placeOrder(id, medsName) {
    this.storeID = id;
    this.medicinePrice = this.searchMeds.fetchMedicinePrice(this.storeID);
    this.showOrder = !this.showOrder;
  }
  confirmOrder(Qty, medsName) {
    const user = JSON.parse(localStorage.getItem('user'));
    const newOrder: IOrder = {
        oId : JSON.parse(localStorage.getItem('Orders')).length + 1,
        sID: this.storeID,
        mID: this.searchMeds.fetchMedicineID(medsName),
        id: user.id,
        oQTY: + this.medicineQty,
        oDate: new Date(),
        oStatus: 'Requested',
        oAmount: this.medicineQty * this.medicinePrice
    };
    if (this.searchMeds.placeOrder(newOrder)) {
      this.router.navigate(['/user-login/dashboard']);
    }
  }
  onKeydown($event) {
    this.onseachClick();
  }
  hideNoStore() {
    this.searchCompleted = false;
  }
  orderHide() {
    this.showOrder = true;
  }
  increaseValue() {
    this.medicineQty++;
  }
  decreaseValue() {
    if (this.medicineQty > 0) {
      this.medicineQty--;
    }
  }
}

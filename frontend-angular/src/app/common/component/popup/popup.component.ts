import { Component, OnInit } from '@angular/core';
import { PopupService } from '../../serivces/popup.service';
@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {
  constructor(private popupService: PopupService) { }
  ngOnInit() {
  }
  hidePopDiv() {
    this.popupService.hide();
  }

  getImageUrl() {
    return this.popupService.getMessage().type === 'success'
      ? 'https://i.gifer.com/XD4x.gif' : 'https://media.giphy.com/media/3og0ItKLUOUzt5uwZW/giphy.gif';
  }
}

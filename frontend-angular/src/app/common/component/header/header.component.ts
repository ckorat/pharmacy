import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent {
  constructor() {}
  imgClick;
  showMenu() {
    document.getElementById("menu").style.width = "300px";
  }

  closeMenu() {
    document.getElementById("menu").style.width = "0px";
  }
}

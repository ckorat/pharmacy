import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { QueryServiceService } from '../../serivces/query-service.service';
import { IQueries } from '../../serivces/queries';
import { PopupService } from '../../serivces/popup.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  queries: IQueries[];
  private cdate: Date = new Date();
  contactUsForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private queryService: QueryServiceService,
              public popup: PopupService) {
    this.contactUsForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.minLength(4), Validators.email]],
      query: ['', [Validators.required, Validators.minLength(8)]],
      description: ['', [Validators.required, Validators.minLength(100)]]
    });

  }

  ngOnInit() {
    this.queries = this.queryService.getQueries();
  }

  onSubmit() {
    const query = new IQueries();
    query.qID = this.queries.length + 1;
    query.qEmailID = this.contactUsForm.controls.email.value;
    query.qSubject = this.contactUsForm.controls.query.value;
    query.qDescription = this.contactUsForm.controls.description.value;
    query.qDate = this.cdate;
    query.qStatus = false;
    query.qreply = '';
    this.queries.push(query);
    this.queryService.setQueries(this.queries);
    this.contactUsForm.reset();
    this.popup.show('success', 'Your Query Has Been Submited to Admin.!');
  }

  get formFields() { return this.contactUsForm.controls; }
}

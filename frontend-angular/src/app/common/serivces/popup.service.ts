import { Injectable } from '@angular/core';
import { Message } from './message.model';

@Injectable({
  providedIn: 'root'
})
export class PopupService {
  private visibleState: boolean;
  message: Message;

  constructor() {
    this.visibleState = false;
    this.message = null;
  }

  hide() {
    this.visibleState = false;
    this.message = null;
  }

  isVisible() {
    return this.visibleState;
  }

  getMessage() {
    return this.message;
  }

  show(type: string, message: string) {
    this.message = new Message(type, message);
    this.visibleState = true;
  }
}

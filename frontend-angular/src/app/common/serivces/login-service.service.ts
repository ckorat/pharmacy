import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUsers } from './users';
import { IStores } from './stores';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {
  stores: IStores[];
  // tslint:disable-next-line: variable-name
  users;
  key = 'USER';
  keyOfStore = 'Store';
  private url = '/assets/data/users.json';
  constructor(private http: HttpClient) {
    if (localStorage.getItem('USER') == null) {
      this.fetchAllUsers();
      this.fetchAllStores();
    }
  }
  // Store
  private storeurl = '/assets/data/store.json';
  getStores(): Observable<IStores[]> {
    return this.http.get<IStores[]>(this.storeurl);
  }
  fetchAllStores() {
    this.getStores()
      .subscribe(data => (
        localStorage.setItem(this.keyOfStore, JSON.stringify(data))
      ));
  }
  setStoreToLocalStorage(stores: IStores[]) {
    localStorage.setItem(this.keyOfStore, JSON.stringify(stores));
  }
  getStoresFromLocalStorage(): IStores[] {
    return JSON.parse(localStorage.getItem(this.keyOfStore));
  }
  checkStore(email: string, password: string): boolean {
    this.stores = this.getStoresFromLocalStorage();
    const store: IStores = this.stores.find(
      data =>
        data.sEmailID === email &&
        data.sPassword === password
    );
    if (store === undefined) {
      return false;
    } else {
      localStorage.setItem('store', JSON.stringify(store));
      this.setStoreToLocalStorage(this.stores);
      return true;
    }
  }
  storeRegistration(store: IStores) {
    this.stores = this.getStoresFromLocalStorage();
    this.stores.push(store);
    this.setStoreToLocalStorage(this.stores);
  }

  getLengthStore(): number {
    this.stores = this.getStoresFromLocalStorage();
    return this.stores.length;
  }
  // User
  getUsers(): Observable<IUsers[]> {
    return this.http.get<IUsers[]>(this.url);
  }
  fetchAllUsers() {
    this.getUsers()
      .subscribe(data => (
        localStorage.setItem(this.key, JSON.stringify(data))
      ));
  }
  setUserToLocalStorage(users: IUsers[]) {
    localStorage.setItem(this.key, JSON.stringify(users));
  }
  getUserFromLocalStorage(): IUsers[] {
    return JSON.parse(localStorage.getItem(this.key));
  }
  checkUser(username: string, password: string): boolean {

    this.users = this.getUserFromLocalStorage();
    const user: IUsers = this.users.find(
      data =>
        data.emailid === username &&
        data.password === password
    );
    if (user === undefined) {
      return false;
    } else {
      localStorage.setItem('user', JSON.stringify(user));
      this.setUserToLocalStorage(this.users);
      return true;
    }
  }
  userRegistration(user: IUsers) {
    this.users = this.getUserFromLocalStorage();
    this.users.push(user);
    this.setUserToLocalStorage(this.users);
  }
  getLengthUser(): number {
    this.users = this.getUserFromLocalStorage();
    return this.users.length;
  }
  checkStores(): IStores[] {

    if (this.getStoresFromLocalStorage() === undefined) {
      this.fetchAllStores();
    }
    const stores = this.getStoresFromLocalStorage();
    return stores;
  }
}


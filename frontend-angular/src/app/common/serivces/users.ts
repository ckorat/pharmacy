export class IUsers {
    id: number;
    fname: string;
    lname: string;
    emailid: string;
    password: string;
    address: string;
    state: string;
    city: string;
    pincode: number;
}
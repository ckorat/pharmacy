export class IQueries {
    qID: number;
    qEmailID: string;
    qSubject: string;
    qDescription: string;
    qDate: Date;
    qStatus: boolean;
    qreply: string;
}

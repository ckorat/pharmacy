import { IAdmin } from './admin';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AdminLoginService {

  private _url = '/assets/data/admin.json';
  constructor(private http: HttpClient) { }

  getAdmin(): Observable<IAdmin[]> {
    return this.http.get<IAdmin[]>(this._url);
  }
}

import { TestBed } from '@angular/core/testing';

import { SearchMedsService } from './search-meds.service';

describe('SearchMedsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchMedsService = TestBed.get(SearchMedsService);
    expect(service).toBeTruthy();
  });
});

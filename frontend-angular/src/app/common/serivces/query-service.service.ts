import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IQueries } from '../../common/serivces/queries';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QueryServiceService {
  queryurl = '/assets/data/queries.json';

  queries: IQueries[];

  constructor(private http: HttpClient) {
  }
  private getAllQuery(): Observable<IQueries[]> {
    return this.http.get<IQueries[]>(this.queryurl);
  }
  fetchAllQueries() {
    this.getAllQuery().subscribe(data => localStorage.setItem('queries', JSON.stringify(data)));
  }

  checkQueries() {
    if (this.getQueries() === null) {
      this.fetchAllQueries();
    }
    const queries = this.getQueries();
    return queries;
  }

  getFilterQueries() {
    this.queries = this.getQueries();
    this.queries = this.queries.filter(data => data.qStatus === false);
    return this.queries;
  }

  setQueries(query: IQueries[]) {
    localStorage.setItem('queries', JSON.stringify(query));
  }
  getQueries(): IQueries[] {
    return JSON.parse(localStorage.getItem('queries'));
  }

}




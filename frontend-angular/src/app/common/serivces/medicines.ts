export class IMedicines {
    mID: number;
    mName: string;
    sID: number;
    mDescription: string;
    mCompanyName: string;
    mType: string;
    mMFD: string;
    mEXD: string;
    mQTY: number;
    mPrice: number;
}

import { IMedicines } from './medicines';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MedicineService {

  private url = '/assets/data/medicine.json';
  key = 'MEDICINES';
  manageMedicines: IMedicines[];
  constructor(private http: HttpClient) {
    this.fetchAllMedicines();
  }
  getMedicines(): Observable<IMedicines[]> {
    return this.http.get<IMedicines[]>(this.url);
  }

  fetchAllMedicines() {
    if (!localStorage.getItem(this.key)) {
       this.getMedicines().subscribe(data => (localStorage.setItem(this.key, JSON.stringify(data))));
    }
  }

  setMedicinesToLocalStorage(medicine: IMedicines[]) {
    localStorage.setItem(this.key, JSON.stringify(medicine));
  }

  getMedicineFromLocalStorage(): IMedicines[] {

    return JSON.parse(localStorage.getItem(this.key));
  }

  addMedicineDetails(medicine: IMedicines) {
    const medicines = this.getMedicineFromLocalStorage();
    medicines.push(medicine);
    this.setMedicinesToLocalStorage(medicines);
  }

  getLengthMedicine(): number {
    const medicines = this.getMedicineFromLocalStorage();
    return medicines.length;
  }
  getMedicineByStoreId(storeId): IMedicines[] {
    this.manageMedicines = this.getMedicineFromLocalStorage();
    if (this.manageMedicines === null) {
      return [];
    } else {
      return this.manageMedicines.filter((data) => data.sID === storeId
      );
    }
  }
  deleteMedicines(id: number, storeId: number) {
    const meds = this.getMedicineFromLocalStorage();
    const i = meds.findIndex(data => data.mID === id && data.sID === storeId);
    meds.splice(i, 1);
    localStorage.setItem(this.key, JSON.stringify(meds));
  }

  updateMedicines(id: number, qty: number, storeId: number) {
    const meds = this.getMedicineFromLocalStorage();
    const i = meds.findIndex(data => data.mID === id && data.sID === storeId);
    meds[i].mQTY = qty;
    localStorage.setItem(this.key, JSON.stringify(meds));
  }
  checkStockofMedicine(mid: number) {
    const medicines = this.getMedicineFromLocalStorage();
    const medicine = medicines.find(data => data.mID === mid);
    return medicine.mQTY;
  }
}

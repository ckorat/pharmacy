export class IOrder {
        oId: number;
        sID: number;
        mID: number;
        id: number;
        oQTY: number;
        oDate: Date;
        oStatus: string;
        oAmount: number;
}

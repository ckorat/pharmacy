import { TestBed } from '@angular/core/testing';

import { ManageMedsService } from './manage-meds.service';

describe('ManageMedsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageMedsService = TestBed.get(ManageMedsService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IStores } from './stores';
import { IUsers } from './users';
import { IMedicines } from './medicines';
import { LoginServiceService } from './login-service.service';
import { IOrder } from './orders';
import { ReturnStatement } from '@angular/compiler';
import { QueryServiceService } from './query-service.service';
@Injectable({
  providedIn: 'root'
})
export class SearchMedsService {
  private url = '/assets/data/store.json';
  private medsurl = '/assets/data/medicine.json';
  private orderurl = '/assets/data/order.json';
  stores: IStores[];
  meds: IMedicines[];
  finalList: IStores[] = [];
  mendsName: string;
  user: IUsers;
  storeID;
  constructor(
    private httpClient: HttpClient,
    private loginService: LoginServiceService,
    private query: QueryServiceService
  ) {
    if (localStorage.getItem('Store') === null) {
      this.getAllStore();
      this.query.fetchAllQueries();
    }
  }
  fetchStore(): Observable<IStores[]> {
    return this.httpClient.get<IStores[]>(this.url);
  }
  // Fethces Stores From LocalStorage
  getAllStore() {
    this.loginService.getStoresFromLocalStorage();
  }
  // Fetching All Medicines For Search from JSON file
  fetchAllMedicines(): Observable<IMedicines[]> {
    return this.httpClient.get<IMedicines[]>(this.medsurl);
  }
  getMeidicineDetails(): IMedicines[] {
    return JSON.parse(localStorage.getItem('MEDICINES'));
  }
  getOrderDetails(): IOrder[] {
    return JSON.parse(localStorage.getItem('Orders'));
  }
  checkAvailableStore(): IStores[] {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.stores = JSON.parse(localStorage.getItem('Store'));
    const listStore = this.stores.filter(store => store.sPincode === this.user.pincode && store.sStatus === true);
    return listStore;
  }
  checkMedsAvailiblity(meds: string): IStores[] {
    this.mendsName = meds;
    return this.checkAvailableStore();
  }
  showStores(stores: IStores[], searchedMeds: string): IStores[] {
    this.meds = this.getMeidicineDetails();
    const finalList = stores.filter(
      store => this.meds.find(med => med.mName === searchedMeds && med.sID === store.sID));
    return finalList;
  }
  fetchMedicinePrice(storeId: number): number {
    this.storeID = storeId;
    let price: number;
    this.meds = this.getMeidicineDetails();
    this.meds.forEach(med => {
      if (med.sID === storeId ) {
        price = med.mPrice;
      }
    });
    return price;
  }
  fetchMedicineID(medsName): number {
    let mID: number;
    this.meds = JSON.parse(localStorage.getItem('MEDICINES'));
    this.meds.forEach(med => {
      if (med.mName === medsName && med.sID === this.storeID ) {
        mID = med.mID;
      }
    });
    return mID;
  }
  placeOrder(newOrder: IOrder): boolean {
    const oldOrder: IOrder[] = this.getOrderDetails();
    oldOrder.push(newOrder);
    localStorage.setItem('Orders', JSON.stringify(oldOrder));
    return true;
  }
}

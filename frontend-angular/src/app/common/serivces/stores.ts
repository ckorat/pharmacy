export class IStores {
        sID: number;
        sName: string;
        sEmailID: string;
        sOwnerName: string;
        sPassword: string;
        sAddress: string;
        sStatus: boolean;
        sPincode: number;
        sContact: number;
}

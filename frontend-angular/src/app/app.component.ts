import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PopupService } from './common/serivces/popup.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Pharmacy-O1';
  router: string;
 
  constructor(private _router: Router, public popup: PopupService ) {
    // this.router=_router.url;
    // console.log(_router.url);
  }
}

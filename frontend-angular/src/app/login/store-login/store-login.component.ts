import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from '../../common/serivces/login-service.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { StoreServicesService } from '../../store/services/store-services.service'
@Component({
  selector: 'app-store-login',
  templateUrl: './store-login.component.html',
  styleUrls: ['./store-login.component.css']
})
export class StoreLoginComponent {
  loginform: FormGroup;
  constructor(private loginservice: LoginServiceService, private fb: FormBuilder, private router: Router) {
    if (localStorage.getItem('store') !== null) {
        this.router.navigateByUrl('/store-login/storedashboard');
    }
    this.loginform = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }
  loginStore() {
    
    const loginStatus = this.loginservice.checkStore(this.loginform.get('email').value, this.loginform.get('password').value);
    if (loginStatus === true) {
      this.router.navigateByUrl('/store-login/storedashboard');
    } else {
      alert('Invalid Email or Password. Please Try again');
    }
  }
  get fval() {
    return this.loginform.controls;
  }

}

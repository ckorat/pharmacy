import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Router } from '@angular/router';
import { LoginServiceService } from '../../common/serivces/login-service.service';
import { IUsers } from '../../common/serivces/users';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { PasswordValidator } from '../../common/Validations/confirmPassword.validator';
import { PopupService } from 'src/app/common/serivces/popup.service';
@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  userLoginForm: FormGroup;
  userRegisterForm: FormGroup;
  submitted = false;

  @ViewChild('registerClass', { static: false }) registerClass: ElementRef;


  constructor(
    private router: Router,
    private loginservice: LoginServiceService,
    private formBuilder: FormBuilder,
    public popup: PopupService
  ) {
    if (localStorage.getItem('user') !== null) {
      this.router.navigate(['/user-login/dashboard']);
    }
    this.userLoginForm = this.formBuilder.group({
      userEmailID: ['', [Validators.required, Validators.email]],
      userPassword: ['', Validators.required]
    });

    this.userRegisterForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      userEmail: ['', [Validators.required, Validators.email]],
      userpassword: ['', [Validators.required, Validators.minLength(8)]],
      userRepassword: ['', [Validators.required, Validators.minLength(8)]],
      userAddress: ['', Validators.required],
      userState: ['', Validators.required],
      userCity: ['', Validators.required],
      userPincode: ['', [Validators.required, Validators.maxLength(6)]]
    },
      {
        validator: PasswordValidator('userpassword', 'userRepassword')
      });

  }
  ngOnInit() {
  }
  logIn() {
    const logStatus = this.loginservice.checkUser(this.userLoginForm.controls.userEmailID.value
      , this.userLoginForm.controls.userPassword.value);
    if (logStatus === true) {
      this.router.navigate(['']);
    } else {
      this.popup.show('error', 'Invalid User Name or Password');
    }
  }
  onKeydown($event) {
    this.logIn();
  }
  showRegister() {
    this.registerClass.nativeElement.style.display = 'block';
  }
  closeRegister() {
    this.registerClass.nativeElement.style.display = 'none';
  }
  userRegistration() {
    this.closeRegister();
    const newUser: IUsers = {
      id: this.loginservice.getLengthUser() + 1,
      fname: this.userRegisterForm.controls.firstName.value,
      lname: this.userRegisterForm.controls.lastName.value,
      emailid: this.userRegisterForm.controls.userEmail.value,
      password: this.userRegisterForm.controls.userpassword.value,
      address: this.userRegisterForm.controls.userAddress.value,
      state: this.userRegisterForm.controls.userState.value,
      city: this.userRegisterForm.controls.userCity.value,
      pincode: this.userRegisterForm.controls.userPincode.value
    };
    this.loginservice.userRegistration(newUser);
    this.router.navigate(['/user-login']);
    this.submitted = true;
    this.userRegisterForm.reset();

  }
  get registerForm() {
    return this.userRegisterForm.controls;
  }
  get loginForm() {
    return this.userLoginForm.controls;
  }

}

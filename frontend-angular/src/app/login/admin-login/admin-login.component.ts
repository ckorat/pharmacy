import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AdminLoginService } from '../../common/serivces/admin-login.service';
import { PopupService } from '../../common/serivces/popup.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit, OnDestroy {
  admins = [];
  adminSubscription;
  @ViewChild('adminId', { static: true }) adminId: ElementRef;
  @ViewChild('adminPassword', { static: false }) password: ElementRef;
  constructor(private router: Router, private adminservice: AdminLoginService,
              public popup: PopupService) {
  }

  ngOnInit() {
    this.adminSubscription = this.adminservice.getAdmin().subscribe(
      data => this.admins = data);
  }
  adminLogin() {
    const admin = this.admins.find(data => data.adminID === this.adminId.nativeElement.value
      && data.adminPassowrd === this.password.nativeElement.value);
    if (admin === undefined) {
      this.popup.show('error', 'Invalid Admin Name Or Password');
    } else {
      this.popup.show('success', 'Admin Loggedin Sucessfully');
      this.router.navigate(['/admin/dashboard']);
      localStorage.setItem('status', 'true' );
    }
  }
  ngOnDestroy(): void {
    this.adminSubscription.unsubscribe();
  }
  onKeydown($event) {
    this.adminLogin();
  }
}

import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../services/admin-service.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  totalStores: number;
  activeStores: number;
  pendingQueries: number;
  totalUsers: number;
  completeOrders: number;
  totalOrders: number;

  constructor(private adminService: AdminServiceService) {
  }

  ngOnInit() {

    this.totalStores = this.adminService.getTotalStores();
    this.activeStores = this.adminService.getActiveStores();
    this.pendingQueries = this.adminService.getPendingQueries();
    this.totalOrders = this.adminService.getTotalOrders();
    this.completeOrders = this.adminService.getCompleteOrders();
    this.totalUsers = this.adminService.getTotalUser();
  }

}

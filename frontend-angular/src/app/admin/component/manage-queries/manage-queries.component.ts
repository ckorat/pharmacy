import { Component, OnInit } from '@angular/core';
import { QueryServiceService } from '../../../common/serivces/query-service.service';
import { IQueries } from '../../../common/serivces/queries';
import { PopupService } from '../../../common/serivces/popup.service';

@Component({
  selector: 'app-manage-queries',
  templateUrl: './manage-queries.component.html',
  styleUrls: ['./manage-queries.component.css']
})
export class ManageQueriesComponent implements OnInit {

  queries: IQueries[];
  constructor(private queryService: QueryServiceService, public popup: PopupService) { }
  ngOnInit() {
    // tslint:disable-next-line: max-line-length

    this.queries = this.queryService.getFilterQueries();
    this.queryService.setQueries(this.queries);
  }

  onReply(id, reply) {
    if (reply === '' ) {
      return ;
    } else {
      const index = this.queries.findIndex(data => data.qID === id);
      this.queries[index].qStatus = true;
      this.popup.show('success', 'Query Has Been Solved');
      this.queries[index].qreply = reply;
      this.queryService.setQueries(this.queries);
      this.queries = this.queryService.getFilterQueries();
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { IStores } from '../../../common/serivces/stores';
import { LoginServiceService } from '../../../common/serivces/login-service.service';



@Component({
  selector: 'app-manage-store',
  templateUrl: './manage-store.component.html',
  styleUrls: ['./manage-store.component.css']
})
export class ManageStoreComponent implements OnInit {

  stores: IStores[];

  constructor(private getStores: LoginServiceService) { }

  ngOnInit() {
   this.stores =  this.getStores.getStoresFromLocalStorage();
   this.stores.sort((store) => store.sStatus ? 1 : -1);
  }
  onVerify(id) {
    const index = this.stores.findIndex(data => data.sID === id);
    this.stores[index].sStatus = !this.stores[index].sStatus;
    this.getStores.setStoreToLocalStorage(this.stores);
  }
}

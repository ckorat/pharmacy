import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  status;
  constructor(public route: ActivatedRoute,public router: Router) { }

  ngOnInit() {
    this.status = localStorage.getItem('status');
  }

  logout() {
    localStorage.setItem('status', 'false');
    this.router.navigate(['/admin']);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdminDashboardComponent } from './component/admin-dashboard/admin-dashboard.component';
import { ManageStoreComponent } from './component/manage-store/manage-store.component';
import { ManageQueriesComponent } from './component/manage-queries/manage-queries.component';
import { HomeComponent } from './component/home/home.component';
@NgModule({
  declarations: [AdminDashboardComponent, ManageStoreComponent, ManageQueriesComponent, HomeComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})


export class AdminModuleModule { }

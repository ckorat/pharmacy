import { Injectable, OnInit } from '@angular/core';
import { LoginServiceService } from 'src/app/common/serivces/login-service.service';
import { QueryServiceService } from 'src/app/common/serivces/query-service.service';
import { UserOrdersService } from 'src/app/user/services/user-services/user-orders.service';
import { IStores } from 'src/app/common/serivces/stores';
import { IQueries } from 'src/app/common/serivces/queries';
import { IOrder } from 'src/app/common/serivces/order';
import { IUsers } from 'src/app/common/serivces/users';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {

  stores: IStores[];
  queries: IQueries[];
  orders: IOrder[];
  users: IUsers[];

  constructor(
    private storeService: LoginServiceService,
    private queryService: QueryServiceService,
    private orderService: UserOrdersService
  ) {
    this.stores = this.storeService.getStoresFromLocalStorage();
    this.orders = JSON.parse(localStorage.getItem('Orders'));
  }
  getTotalStores(): number {
    return this.stores.length;
  }

  getTotalUser(): number {
    this.storeService.fetchAllUsers();
    const user = this.storeService.getUserFromLocalStorage();
    return user.length;
  }
  getTotalOrders(): number {
    return this.orders.length;
  }

  getActiveStores(): number {
    return this.storeService.getStoresFromLocalStorage().filter(data => data.sStatus === true).length;
  }
  getPendingQueries(): number {
    this.queries = this.queryService.getQueries();
    return this.queries.filter(data => data.qStatus === false).length;
  }

  getCompleteOrders(): number {
    return this.orders.filter(data => data.oStatus === 'Completed').length;
  }

}

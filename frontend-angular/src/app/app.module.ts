import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotfoundComponent } from './common/Component/notfound/notfound.component';
import { HeaderComponent } from './common/Component/header/header.component';
import { AdminModuleModule } from './admin/admin-module.module';
import { UserModuleModule } from './user/user-module.module';
import { StoreModuleModule } from './store/store-module.module';
import { AdminLoginComponent } from './login/admin-login/admin-login.component';
import { UserLoginComponent } from './login/user-login/user-login.component';
import { StoreLoginComponent } from './login/store-login/store-login.component';
import { LoginModuleModule } from './login/login-module.module';
import { SearchComponent } from './common/component/search/search.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ContactUsComponent } from './common/component/contact-us/contact-us.component';
import { AboutUsComponent } from './common/component/about-us/about-us.component';
import { UserProfileComponent } from './user/component/user-profile/user-profile.component';
import { FooterComponent } from './common/component/footer/footer.component';
import { PopupComponent } from './common/component/popup/popup.component';

@NgModule({
  declarations: [
    AppComponent,
    NotfoundComponent,
    HeaderComponent,
    SearchComponent,
    AboutUsComponent,
    FooterComponent,
    ContactUsComponent,
    PopupComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AdminModuleModule,
    UserModuleModule,
    StoreModuleModule,
    LoginModuleModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

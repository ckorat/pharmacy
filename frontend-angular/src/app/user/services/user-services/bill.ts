export class IviewBill {
    id: number;
    status: string;
    orderDate: Date;
    sName: string;
    sEmailID: string;
    sAddress: string;
    sPincode: number;
    sContact: number;
    mName: string;
    mCompanyName: string;
    mType: string;
    mQTY: number;
    mPrice: number;
    Amount: number;
  }
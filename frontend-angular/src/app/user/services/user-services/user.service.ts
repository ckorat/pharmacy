import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IUsers } from '../../../common/serivces/users';
import { IOrder } from '../../../common/serivces/order';
import { Router } from '@angular/router';
import { LoginServiceService } from '../../../common/serivces/login-service.service';

@Injectable({
  providedIn: 'root'
})
export class UserService implements OnDestroy {
  constructor(private http: HttpClient, private router: Router, private loginservice: LoginServiceService) {
  }
  key = 'USER';
  path = '/assets/data/users.json';
  ds: Subscription;

  getAllUsers() {
   this.loginservice.getUserFromLocalStorage();
  }
  getUserDetail() {
    return JSON.parse(localStorage.getItem('user'));
  }

  updateUserDetails(u: IUsers) {

    const users: IUsers[] = JSON.parse(localStorage.getItem(this.key));
    const i = users.findIndex(data => data.id === u.id);
    users[i].fname = u.fname;
    users[i].lname = u.lname;
    users[i].address = u.address;
    users[i].city = u.city;
    users[i].state = u.state;
    users[i].pincode = u.pincode;
    localStorage.setItem(this.key, JSON.stringify(users));
    localStorage.setItem('user', JSON.stringify(u));
  }

  updateUserPassword(id: number, oldPassword: string, newPassword: string): boolean {
    const users: IUsers[] = JSON.parse(localStorage.getItem(this.key));
    const i = users.findIndex(data => data.id === id);

    if (oldPassword === users[i].password) {
      users[i].password = newPassword;
      localStorage.setItem(this.key, JSON.stringify(users));

      localStorage.setItem('user', JSON.stringify(users[i]));
      return true;
    }
    return false;
  }


logout() {
  localStorage.removeItem('user');
  this.router.navigate(['/user-login']);
}

ngOnDestroy() {
  this.ds.unsubscribe();
}
}



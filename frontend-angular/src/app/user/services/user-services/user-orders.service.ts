import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IOrder } from '../../../common/serivces/order';
import { Subscription } from 'rxjs';
import { IMedicines } from '../../../common/serivces/medicines';
import { IUsers } from 'src/app/common/serivces/users';
import { IMOrder } from './mOrder';
import { LoginServiceService } from '../../../common/serivces/login-service.service';
import { IStores } from '../../../common/serivces/stores';
import { IviewBill } from './bill';

@Injectable({
  providedIn: 'root'
})
export class UserOrdersService implements OnDestroy {

  private url = '/assets/data/order.json';
  private mUrl = '/assets/data/medicine.json';
  ds: Subscription;
  user: IUsers;
  allOrders: IMOrder[];
  recentOrders: IMOrder[];
  completeOrders: IMOrder[];
  key = 'Orders';
  iviewBill: IviewBill;

  constructor(private http: HttpClient, private loginService: LoginServiceService) {

  }

  getOrderByUserId(id: number): IOrder[] {

    const orders: IOrder[] = JSON.parse(localStorage.getItem(this.key));
    return orders.filter(data => data.id === id);

  }
  getMedicineDetail() {
    this.ds = this.http.get<IMedicines>(this.mUrl)
      .subscribe(data => localStorage.setItem('MEDICINES', JSON.stringify(data))
      );

  }
  getMedicineDetailById(mId: number) {
    const medicines: IMedicines[] = JSON.parse(localStorage.getItem('MEDICINES'));
    return medicines.find(data => data.mID === mId);


  }
  getStoreDetailById(sId: number) {
    const stores: IStores[] = JSON.parse(localStorage.getItem('Store'));
    return stores.find(data => data.sID === sId);


  }

  getOrderDetailAll(uId): IMOrder[] {
    const userOrder: IOrder[] = this.getOrderByUserId(uId);
    const stores: IStores[] = this.loginService.getStoresFromLocalStorage();
    let store: IStores;
    const orders = new Array<IMOrder>();
    userOrder.forEach((data) => {
      const order = new IMOrder();
      order.oId = data.oId;
      store = stores.find(storename => storename.sID === data.sID);
      order.storeName = store.sName;
      order.sId = data.sID;
      order.medicine = this.getMedicineDetailById(data.mID);
      order.uid = data.id;
      order.oQTY = data.oQTY;
      order.oDate = data.oDate;
      order.oAmount = data.oAmount;
      order.oStatus = data.oStatus;
      orders.push(order);
    });
    this.allOrders = orders;
    return this.allOrders;
  }
  getRecentOrders(allOrders) {
    this.recentOrders = allOrders.filter(data => data.oStatus === 'Requested');
    return this.recentOrders;
  }

  getCompleteOrders(allOrders) {
    this.completeOrders = allOrders.filter(data => data.oStatus === 'Completed' ||  data.oStatus === 'Canceled');
    return this.completeOrders;
  }
  getBill(order: IMOrder) {
    let bill = new IviewBill();
    const stores = this.getStoreDetailById(order.sId);
    const medicines = this.getMedicineDetailById(order.medicine.mID);
    bill.id = order.oId;
    bill.status = order.oStatus;
    bill.orderDate = order.oDate;
    bill.sName = stores.sName;
    bill.sEmailID = stores.sEmailID;
    bill.sContact = stores.sContact;
    bill.sAddress = stores.sAddress;
    bill.sPincode = stores.sPincode;
    bill.mName = medicines.mName;
    bill.mCompanyName = medicines.mCompanyName;
    bill.mType = medicines.mType;
    bill.mQTY = order.oQTY;
    bill.mPrice = medicines.mPrice;
    bill.Amount = order.oAmount;
    return bill;
  }
  ngOnDestroy() {
    this.ds.unsubscribe();
  }

}



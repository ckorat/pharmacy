import { IMedicines } from 'src/app/common/serivces/medicines';

export class IMOrder {
    oId: number;
    storeName: string;
    sId: number;
    medicine: IMedicines;
    mPrice: number;
    uid: number;
    oQTY: number;
    oDate: Date;
    oStatus: string;
    oAmount: number;
}

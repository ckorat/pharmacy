import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserOrdersService } from '../../services/user-services/user-orders.service';
import { IUsers } from '../../../common/serivces/users';
import { IMOrder } from '../../services/user-services/mOrder';
import { UserService } from '../../services/user-services/user.service';
import { IStores } from 'src/app/common/serivces/stores';
import { IMedicines } from 'src/app/common/serivces/medicines';
import { IviewBill } from '../../services/user-services/bill';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {

  user: IUsers;
  hideBill = false;
  allOrders: IMOrder[];
  recentOrders: IMOrder[];
  completeOrders: IMOrder[];
  bill: IviewBill;
  constructor(private router: Router, private userOrderService: UserOrdersService, private userService: UserService) {

    if (localStorage.getItem('user') === null) {
      this.router.navigate(['/user-login']);
    } else {
      this.user = JSON.parse(localStorage.getItem('user'));
      this.allOrders = this.userOrderService.getOrderDetailAll(this.user.id);
      this.recentOrders = this.userOrderService.getRecentOrders(this.allOrders);
      this.completeOrders = this.userOrderService.getCompleteOrders(this.allOrders);
    }
  }
  ngOnInit(): void {
    if (localStorage.getItem('MEDICINES') === null) {
      this.userOrderService.getMedicineDetail();
    }
  }



  viewBill(order) {

    this.bill = this.userOrderService.getBill(order);
    this.hideBill = true;

  }
  logout() {
    this.userService.logout();
  }
}


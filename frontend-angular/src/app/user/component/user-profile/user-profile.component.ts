import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, NgControl } from '@angular/forms';
import { UserService } from '../../services/user-services/user.service';
import { IUsers } from '../../../common/serivces/users';
import { Router } from '@angular/router';
import { PasswordValidator } from '../../../common/Validations/confirmPassword.validator';
import { PopupService } from '../../../common/serivces/popup.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  flag = false;
  userProfileForm: FormGroup;
  changePasswordForm: FormGroup;
  userModel: IUsers;
  constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router,
              public popup: PopupService) {

    this.userProfileForm = this.formBuilder.group({
      id: [''],
      fname: ['', [Validators.required]],
      lname: ['', [Validators.required]],
      emailid: [{ value: '', disabled: true }, [Validators.required]],
      password: ['', [Validators.required]],
      address: ['', [Validators.required]],
      state: ['', [Validators.required]],
      city: ['', [Validators.required]],
      pincode: ['', [Validators.required]]
    });

    this.changePasswordForm = this.formBuilder.group({
      oldPass: ['', [Validators.required]],
      newPass: ['', [Validators.required, Validators.minLength(8)]],
      confirmPass: ['', [Validators.required]]
    }, {
      validator: PasswordValidator('newPass', 'confirmPass')
    });
    this.userModel = this.userService.getUserDetail();
    this.userProfileForm.patchValue({
      id: this.userModel.id,
      fname: this.userModel.fname,
      lname: this.userModel.lname,
      emailid: this.userModel.emailid,
      password: this.userModel.password,
      address: this.userModel.address,
      state: this.userModel.state,
      city: this.userModel.city,
      pincode: this.userModel.pincode
    });

  }

  ngOnInit() {

  }
  onSaveProfile() {

    this.userModel = this.userProfileForm.value;
    this.userService.updateUserDetails(this.userModel);
    this.popup.show('success', 'Your Profile is Updated');
  }

  onSavePassword() {
    const oldPass = this.changePasswordForm.controls.oldPass.value;
    const newPass = this.changePasswordForm.controls.newPass.value;
    const flag = this.userService.updateUserPassword(this.userModel.id, oldPass, newPass);
    flag ? this.popup.show('success', 'Password changed successfully') : this.popup.show('error', 'Old password is wrong.');
  }

  get formFields() { return this.userProfileForm.controls; }
  get passwordFormFields() { return this.changePasswordForm.controls; }

  logout() {
    this.userService.logout();
  }

}
